package kcallac.mcpackdowngrader.console;

/**
 * Classe contenant des méthodes statiques d'affichage de messages dans la console.
 * 
 * @author Killian CALLAC
 *
 */
public class ConsolePrinter {
	
	/**
	 * Affiche un message d'erreur comprenant la pile d'appel d'un objet <code>Throwable</code>.
	 * 
	 * @param t Objet <code>Throwable</code> dont la pile d'appel doit être affichée.
	 * 
	 * @see <code>{@link java.lang.Throwable}</code>.
	 * 
	 */
	public static void printThrowable(Throwable t) {
		
		System.err.println("Une erreur est survenue :");
		t.printStackTrace();
		
	}

}
