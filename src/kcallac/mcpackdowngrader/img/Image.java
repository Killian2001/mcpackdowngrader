package kcallac.mcpackdowngrader.img;

import java.awt.image.BufferedImage;

/**
 * Classe représentant une image en format 32 bits / ARGB.<br>
 * Hérite de <code>{@link java.awt.image.BufferedImage}</code>.
 * 
 * @author Killian CALLAC
 * 
 * @see <code>{@link java.awt.image.BufferedImage}</code>
 *
 */
public class Image extends BufferedImage {

	/**
	 * Constructeur de la classe.
	 * 
	 * @param w Longueur de l'image.
	 * @param h Hauteur de l'image.
	 */
	public Image(int w, int h) {

		// Initialisation en format 32 bits / ARGB.
		super(w, h, TYPE_INT_ARGB);
		
	}
	
}
