package kcallac.mcpackdowngrader.img;

/**
 * Représente des coordonées entières sur un plan.
 * 
 * @author Killian CALLAC
 *
 */
public class Coords {

	private int x = 0;

	private int y = 0;
	
	/*
	 * Constructeur par défaut de la classe.
	 */
	public Coords() { }
	
	/**
	 * Constructeur de la classe avec spécification des coordonées
	 * 
	 * @param x Abscisse.
	 * @param y Ordonnée.
	 * 
	 * @throws IllegalArgumentException Si <code>x</code> ou <code>y</code> est négatif.
	 */
	public Coords(int x, int y) {
		
		setX(x);
		setY(y);
		
	}
	
	/**
	 * Accesseur de l'abscisse.
	 * 
	 * @return Abscisse.
	 */
	public int getX() {
		
		return x;
		
	}

	/**
	 * Mutateur de l'abscisse.
	 * 
	 * @param x Nouvelle abscisse.
	 * 
	 * @throws IllegalArgumentException Si <code>x</code> est négatif.
	 */
	public void setX(int x) {
		
		if (x < 0) throw new IllegalArgumentException("x doit être positif.");
		
		this.x = x;
		
	}

	/**
	 * Accesseur de l'ordonnée.
	 * 
	 * @return Ordonnée.
	 */
	public int getY() {
		
		return y;
		
	}

	/**
	 * Mutateur de l'ordonnée.
	 * 
	 * @param y Nouvelle ordonnée.
	 * 
	 * @throws IllegalArgumentException Si <code>y</code> est négatif.
	 */
	public void setY(int y) {
		
		if (y < 0) throw new IllegalArgumentException("y doit être positif.");
		
		this.y = y;
		
	}
	
}
