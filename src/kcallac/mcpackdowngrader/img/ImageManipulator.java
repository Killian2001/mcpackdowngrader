package kcallac.mcpackdowngrader.img;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Classe contenant des méthodes statiques de manipulation d'image
 * nécessaires au programme.
 * 
 * @author Killian CALLAC
 *
 */
public class ImageManipulator {
	
	/**
	 * Méthode dessinant sur une image une sous-image dont le pixel de d'angle supérieur gauche
	 * correspond à une position spécifiée en paramètre.<br>
	 * Les pixels de la sous-image sortant de l'image ne seront simplement pas dessinés.
	 * 
	 * @param img1 Image.
	 * @param img2 Sous-image.
	 * @param c Coordonnées du pixel de l'angle supérieur gauche de <code>img2</code>.
	 */
	public static void drawImage(Image img1, Image img2, Coords c) {
		
		// Parcours de l'image img2.
		for (int x = 0; x < img2.getWidth(); x++) {
			
			for (int y = 0; y < img2.getHeight(); y++) {
				
				// Coordonnées du pixel à écrire sur img1.
				int img1X = c.getX() + x, img1Y = c.getY() + y;
				
				// Si le pixel (img1X, img1Y) appartient à img1, on écrit. 
				if (img1X < img1.getWidth() && img1Y < img1.getHeight()) 
					img1.setRGB(img1X, img1Y, img2.getRGB(x, y));
				
			}
			
		}
		
	}
	
	/**
	 * Écrit une image au format PNG dans le fichier spécifié.
	 * 
	 * @param f Fichier à écrire.
	 * @param img Image à écrire dans le fichier.
	 * 
	 * @throws IOException En cas de problème à l'ouverture du fichier <code>f</code>.
	 */
	public static void writeImage(File f, Image img) throws IOException {
		
		ImageIO.write(img, "png", f);
		
	}
	

}
