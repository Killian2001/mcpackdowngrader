package kcallac.mcpackdowngrader;

import kcallac.mcpackdowngrader.console.ConsolePrinter;
import kcallac.mcpackdowngrader.gui.MainWindow;
import kcallac.mcpackdowngrader.gui.MessageBox;

/**
 * Classe principale du programme, contient le point d'entrée.
 * 
 * @author Killian CALLAC
 *
 */
public class MCPackDowngrader {
	
	/* CONSTANTES */
	
	/**
	 * Numéro de version majeur.
	 */
	public static final int MAJ_VERSION = 0;
	
	/**
	 * Numéro de version mineur.
	 */
	public static final int MIN_VERSION = 0;
	
	/**
	 * Numéro de révision.
	 */
	public static final int REV_VERSION = 4;
	
	/**
	 * Numéro de version de développement.
	 */
	public static final int DEV_VERSION = 0;
	
	/* --- */

	public static void main(String[] args) {
		
		try {
			
			System.out.println("Hello, World !");
			
			new MainWindow();	// Création fenêtre principale.
			
			// TODO Démarrage programme.
			
			//throw new Throwable("test");
			
		} catch (Throwable t) {	// En cas d'erreur ou d'exception...
			
			// Affichage d'un message d'erreur.
			
			// Affichage message fenêtre.
			MessageBox.showThrowableBox(t);
			
			// Affichage message dans la console.
			ConsolePrinter.printThrowable(t);
			
			System.exit(1);
			
		}

	}

}
