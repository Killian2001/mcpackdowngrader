package kcallac.mcpackdowngrader.io.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import kcallac.mcpackdowngrader.io.FileUtils;

/**
 * Classe représentant une archive ZIP, pouvant être lue et écrite.
 * 
 * @author Killian CALLAC.
 *
 */
public class ZipArchive {
	
	// Fichier ZIP.
	private File zipFile = null;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param zipFile Fichier ZIP.
	 */
	public ZipArchive(File zipFile) {
		
		this.zipFile = zipFile;
		
	}
	
	/**
	 * Extrait un fichier dans un répértoire donné.<br>
	 * Si le répértoire existe déjà, il est effacé puis recréé.
	 * 
	 * @param dir Répértoire dans lequel extraire le ficihier.
	 * 
	 * @throws IOException En cas de problème de lecture / d'écriture d'un répértoire ou d'un fichier.
	 * @throws FileNotFoundException Si <code>dir</code> n'est pas un répértoire.
	 * @throws ZipException En cas d'erreur de format ZIP.
	 */
	public void extract(File dir) throws IOException, FileNotFoundException, ZipException {
		
		// Vérification existence du répértoire.
		if (dir.exists()) {
			
			if (!dir.isDirectory()) throw new FileNotFoundException("dir doit être un répértoire.");
			
			// Suppression répértoire
			FileUtils.removeDir(dir);
			
		}
		
		
		// Recréation
		dir.mkdir();
		
		// Parcours des entrées de l'archive.
		try (ZipFile zf = new ZipFile(zipFile)) {
			
			Iterator<? extends ZipEntry> it = zf.entries().asIterator();
			
			while (it.hasNext()) {
				
				// Extraction de l'entrée.
				extractFile(it.next().getName(), dir);
				
			}
			
		}
			
	}
	
	/**
	 * Extrait un fichier de chemin donné de l'archive ZIP.
	 * 
	 * @param path Chemin du fichier à extraire dans l'archive ZIP.
	 * @param dir Dossier de dans lequel le fichier sera extrait.
	 * 
	 * @return Le fichier extrait.
	 * 
	 * @throws IOException En cas de problème de lecture / d'écriture d'un fichier ou d'un répértoire.
	 * @throws FileNotFoundException Si <code>dir</code> ou le fichier de l'archive ZIP de chemin 
	 * <code>path</code> n'existe pas, ou si <code>dir</code> n'est pas un répértoire.
	 * @throws ZipException En cas d'erreur de format ZIP.
	 */
	public File extractFile(String path, File dir) throws IOException, FileNotFoundException, ZipException {
		
		// Vérification existence du répértoire.
		if (dir.exists()) {
			
			if (!dir.isDirectory()) throw new FileNotFoundException("dir doit être un répértoire.");
			
		}
		
		// Extraction du fichier.
		try (ZipFile zf = new ZipFile(zipFile)) {
			
			// Entrée correspondant à path.
			ZipEntry entry = zf.getEntry(path);
			
			// Si l'entrée n'existe pas...
			if (entry == null) throw new FileNotFoundException("Fichier compressé " + path + " non trouvé.");
			
			// Lecture de l'entrée...
			try (InputStream i = zf.getInputStream(entry)) {
				
				// Création fichier / répértoire de l'entrée.
				File f = new File(dir.getAbsolutePath() + "/" + entry.getName());
				
				if (entry.isDirectory()) FileUtils.createMaybeExistantDir(f);
				else {
			
					// Si fichier, écriture fichier.
					FileUtils.createDirFile(f);
					FileUtils.write(f, i.readAllBytes());
				
				}
				
				return f;
			}
		} 
	}
	
	/**
	 * Compresse un répértoire dans l'archive ZIP.
	 * Réécrit l'archive ZIP si elle existe déjà.
	 * 
	 * @param dir Répértoire à compresser.
	 * 
	 * @throws IOException En cas de problème de lecture / d'écriture d'un fichier ou d'un répértoire.
	 * @throws FileNotFoundException Si <code>dir</code> n'existe pas ou n'est pas un répértoire.
	 * @throws ZipException En cas d'erreur de format ZIP.
	 */
	public void compress(File dir) throws IOException, FileNotFoundException, ZipException {
		
		// Si le répértoire à compresser existe...
		if (dir.exists()) {
			
			// Si n'est pas un fichier...
			if (!dir.isDirectory()) throw new FileNotFoundException("dir doit être un répértoire.");
			
		} else throw new FileNotFoundException("Le répértoire dir n'existe pas.");
				
		// Compression récursive du répértoire.
		
		try (
				FileOutputStream out = new FileOutputStream(zipFile);
				ZipOutputStream zout = new ZipOutputStream(out);
		) {
			
			compress(dir, "", zout);
			
		}
	
	}
	
	/**
	 * Méthode privée d'écriture d'un répértoire dans un fichier ZIP.
	 * 
	 * @param dir Fichier a écrire.
	 * @param prePath Chemin relatif à l'archive d'accès du répértoire parent du fichier.
	 * @param zout Flux d'écriture de l'archive.
	 * 
	 * @throws IOException En cas de problème de lecture / d'écriture d'un fichier ou d'un répértoire.
	 * @throws FileNotFoundException Si un fichier n'existe pas.
	 * @throws ZipException En cas d'erreur de format ZIP.
	 */
	private void compress(File dir, String prePath, ZipOutputStream zout) 
			throws IOException, FileNotFoundException, ZipException {
		
		// Parcours des fichiers du répértoire.
		for (File f : dir.listFiles()) {
			
			// Si sous-répértoire, appel récursif...
			if (f.isDirectory()) {
			
				compress(f, prePath + f.getName() + "/", zout);
				
			} else {
				
				// ... sinon écriture fichier dans l'archive.
				try (FileInputStream fin = new FileInputStream(f)) {
					
					// Création de l'entrée.
					ZipEntry ze = new ZipEntry(prePath + f.getName());
					
					// Buffer contenant les données a écrire.
					byte[] buffer = fin.readAllBytes();
					
					// Écriture de l'entrée.
					zout.putNextEntry(ze);
					zout.write(buffer, 0, buffer.length);
					
					// Fermeture de l'entrée.
					zout.closeEntry();
				
				}
				
			}
			
		}
		
	}
	
}
