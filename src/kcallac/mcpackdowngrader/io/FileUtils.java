package kcallac.mcpackdowngrader.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Classe contenant des méthodes statiques facilitant la gestion de l'écriture / lecture de fichiers et
 * des répértoires.
 * 
 * @author Killian CALLAC
 *
 */
public class FileUtils {
	
	/**
	 * Créée un nouveau fichier.
	 * 
	 * @param f Fichier à créer.
	 * @throws IOException Si le fichier existe déjà.
	 */
	public static void create(File f) throws IOException {
		if (!f.createNewFile()) throw new IOException("Le fichier " + f.getAbsolutePath() + " existe déjà.");
	}
	
	/**
	 * Écrit une chaîne d'octets dans un fichier.
	 * 
	 * @param f Fichier à écrire.
	 * @param data Données à écrire.
	 * @throws IOException En cas de problème lors de la manipulation du fichier.
	 */
	public static void write(File f, byte[] data) throws IOException {
		
		try (FileOutputStream out = new FileOutputStream(f)) {
			
			out.write(data);
			
		}
		
	}
	
	/**
	 * Crée un répertoire ainsi que ses répértoires parents.
	 * 
	 * @param dir Répértoire à créer.
	 * @throws IOException Si la création du répértoire échoue.
	 */
	public static void mkdirs(File dir) throws IOException {
		boolean val = dir.mkdirs();
		if (!val) throw new IOException("Échec a la création du répértoire " + dir.getAbsolutePath());
	}
	
	/**
	 * Supprime un fichier.
	 * 
	 * @param dir Fichier a supprimer.
	 * @throws IOException Si la suppression du fichier échoue.
	 */
	public static void delete(File f) throws IOException {
		if (!f.delete()) throw new IOException("Échec de la suppression du fichier " + f.getAbsolutePath() + ".");
	}
	
	/**
	 * Supprime récursivement un répértoire.
	 * 
	 * @param dir Répértoire à supprimer.
	 * 
	 * @throws IOException En cas d'échec de suppression d'un fichier.
	 * @throws FileNotFoundException Si <code>dir</code> n'existe pas ou n'est pas un répértoire.
	 */
	public static void removeDir(File dir) throws IOException, FileNotFoundException {
		
		// Vérification existence du répértoire.
		if (!dir.exists()) throw new FileNotFoundException("Le répértoire " + dir.getAbsolutePath() + " n'existe pas");
		
		// dir est-il un répértoire ?
		if (!dir.isDirectory()) 
			throw new FileNotFoundException(dir.getAbsolutePath() + " n'est pas un répértoire.");
		
		
		// Vidage de répértoire avant suppression : suppression récursive des fichiers et sous-répértoire.
		for (File f : dir.listFiles()) {
			
			// Appel récursif si f est un répértoire, sinon suppression classique.
			if (f.isDirectory()) removeDir(f);
			else delete(f);
			
		}
		
		// Suppression du répértoire.
		delete(dir);
		
	}
	
	/**
	 * Crée un répértoire d'après le fichier spécifié.
	 * À utiliser dans le cas où il est possible que le répértoire existe déjà.
	 * 
	 * @param dir Répértoire à créer.
	 * @throws IOException En cas d'échec à la création du répértoire.
	 */
	public static void createMaybeExistantDir(File dir) throws IOException {
		
		// Vérif existence répértoire.
		if (!dir.exists()) mkdirs(dir);
		
	}
	
	/**
	 * Crée un fichier et ses répértoires parents.
	 * Les répértoires parents peuvent déjà exister.
	 * 
	 * @param f Fichier à créer.
	 * @throws IOException En cas d'erreur à la création des répértoires ou du fichier.
	 */
	public static void createDirFile(File f) throws IOException {
		
		// Chemin absolu du fichier.
		String fAbs = f.getAbsolutePath();
		
		// On prend l'indice du dernier séparateur répértoire/répértoire ou répértoire/fichier (/ ou \).
		int lastSlashIndex = Math.max(fAbs.lastIndexOf('/'), fAbs.lastIndexOf('\\'));
		
		// Création des répértoires à partir de l'indice du dernier séparateur.
		if (lastSlashIndex != -1) createMaybeExistantDir(new File(fAbs.substring(0, lastSlashIndex + 1)));
		
		// Création fichier.
		create(f);
		
	}

}
