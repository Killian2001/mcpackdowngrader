package kcallac.mcpackdowngrader.gui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Classe contenant des méthodes statiques permettant de faciliter l'affichage des boîtes de messages.
 * 
 * @author Killian CALLAC.
 *
 */
public class MessageBox {
	
	/**
	 * Affiche une boîte de message d'erreur.
	 * 
	 * @param msg Message de l'erreur.
	 */
	public static void showErrorBox(String msg) {
		
		JOptionPane.showMessageDialog(new JFrame(), msg, "Erreur", JOptionPane.ERROR_MESSAGE);
		
	}
	
	/**
	 * Affiche le message d'erreur d'un objet <code>Throwable</code> dans une boîte de message d'erreur.
	 * 
	 * @param t Objet <code>Throwable</code>
	 * 
	 * @see <code>{@link java.lang.Throwable}</code>
	 */
	public static void showThrowableBox(Throwable t) {
		
		MessageBox.showErrorBox("Une erreur est survenue :\n" + t.toString());
		
	}

}
