package kcallac.mcpackdowngrader.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import kcallac.mcpackdowngrader.MCPackDowngrader;

/**
 * Classe définissant la fenêtre principale du programe.<br>
 * Hérite de <code>{@link javax.swing.JFrame}</code>.
 * 
 * @author Killian CALLAC
 * 
 * @see <code>{@link javax.swing.JFrame}</code>
 *
 */
public class MainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5547294344245785042L;
	
	/* CONSTANTES FENÊTRE */
	
	/**
	 * Longueur de la fenêtre.
	 */
	private static final int WINDOW_W = 600;
	
	/**
	 * Hauteur de la fenêtre.
	 */
	private static final int WINDOW_H = 150;
	
	/* --- */
	
	/* COMPOSANTS DE LA FENÊTRE */
	
	// Content panel.
	
	/**
	 * JPanel contenant tous les composants de la fenêtre.
	 */
	private JPanel contentPan = new JPanel();
	
	// Champs de texte.
	
	/**
	 * Champ chemin d'accès du resource pack source.
	 */
	private JTextField sourcePathField = new JTextField();
	
	/**
	 * Champ chemin d'accès du texture pack source.
	 */
	private JTextField destPathField = new JTextField();
	
	// Boutons
	
	/**
	 * Bouton d'affichage de la boîte de dialogue de séléction du chemin d'accès du resource pack source.
	 */
	private JButton selSrcPath = new JButton("...");
	
	/**
	 * Bouton d'affichage de la boîte de dialogue de séléction du chemin d'accès du texture pack destination.
	 */
	private JButton selDestPath = new JButton("...");
	
	/**
	 * Bouton de validation.
	 */
	private JButton submit = new JButton("Valider");
	
	// Labels
	
	/**
	 * Label champ chemin d'accès du resource pack source.
	 */
	private JLabel srcLabel = new JLabel("Resource pack source : ", SwingConstants.RIGHT);
	
	/**
	 * Label champ chemin d'accès du texture pack destination.
	 */
	private JLabel destLabel = new JLabel("Texture pack destination : ", SwingConstants.RIGHT);
	
	/* --- */
	
	/**
	 * Constructeur de la fenêtre.
	 */
	@SuppressWarnings("unused")	// Suppression avertissement dead code ligne 54.
	public MainWindow() {
		
		// Appel du constructeur de la classe mère avec définition du texte de la barre de titre.
		super(
				"MCPackDowngrader v." + MCPackDowngrader.MAJ_VERSION
				+ "." + MCPackDowngrader.MIN_VERSION + "." + MCPackDowngrader. REV_VERSION
				+ ((MCPackDowngrader.DEV_VERSION != 0 ) ? "_" + MCPackDowngrader.DEV_VERSION + " [dev]" : "")
		);
		
		// Ajout listener boutons.
		Listener l = new Listener();
		
		selSrcPath.addActionListener(l);
		selDestPath.addActionListener(l);
		submit.addActionListener(l);
		
		// Ajout layout sur contentPan.
		contentPan.setLayout(new GridBagLayout());
		
		// Ajout composants avec contraintes layout.
		
		// Taille des barres de texte.
		final double FIELD_WEIGHT = 128;
		
		// Objet stockant les contraintes.
		GridBagConstraints c = new GridBagConstraints();
		
		// Remplissage de la eone.
		c.fill = GridBagConstraints.BOTH;
		
		// Tailles par rapport aux axes x et y.
		c.weightx = 1;
		c.weighty = 1;
		
		// srcLabel
		c.gridx = 1;
		c.gridy = 1;
		contentPan.add(srcLabel, c);
		
		// sourcePathField
		c.weightx = FIELD_WEIGHT;
		c.gridx = 2;
		contentPan.add(sourcePathField, c);
		
		// selSrcPath
		c.weightx = 1;
		c.gridx = 3;
		contentPan.add(selSrcPath, c);
		
		// destLabel
		c.gridx = 1;
		c.gridy = 2;
		contentPan.add(destLabel, c);
		
		// destPathField
		c.weightx = FIELD_WEIGHT;
		c.gridx = 2;
		contentPan.add(destPathField, c);
		
		// selDestPath
		c.weightx = 1;
		c.gridx = 3;
		contentPan.add(selDestPath, c);
		
		// submit
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 3;
		contentPan.add(submit, c);
		
		// Réglage taille composants.
		
		// Boutons sel
		sourcePathField.setSize(100, 10);
		destPathField.setSize(100, 10);
		
		// contentPan comme panel principal.
		setContentPane(contentPan);
		
		// Taille fenêtre.
		setSize(WINDOW_W, WINDOW_H);
		
		// Comportement de fermeture.
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// Non redimensionnable.
		setResizable(false);
		
		// Visibilité.
		setVisible(true);
	}
	
	/**
	 * Classe d'écoute des actions utilisateur sur les composants de la fenêtre.<br>
	 * Implémente l'interface <code>{@link java.awt.event.ActionListener}</code>.
	 * 
	 * @author Killian CALLAC
	 * 
	 * @see <code>{@link java.awt.event.ActionListener}</code>
	 *
	 */
	private class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			// Composant ayant été actionné par l'utilisateur.
			Object srcComp = e.getSource();
			
			if (srcComp == selSrcPath) {
				
				// Choix du pack source par boite de dialogue.
				
				// Création boite de dialogue choix de fichier.
				JFileChooser chooser = new JFileChooser();
				
				// Si fichier seléctionné, mise à jour de sourcePathField avec le chemin absolu du fichier.
				if (chooser.showOpenDialog(new JFrame()) == JFileChooser.APPROVE_OPTION)
					sourcePathField.setText(chooser.getSelectedFile().getAbsolutePath());
				
			} else if (srcComp == selDestPath) {
				
				// Choix du pack destination par boite de dialogue.
				
				// Création boite de dialogue choix de fichier.
				JFileChooser chooser = new JFileChooser();
				
				// Si fichier seléctionné, mise à jour de sourcePathField avec le chemin absolu du fichier.
				if (chooser.showSaveDialog(new JFrame()) == JFileChooser.APPROVE_OPTION)
					destPathField.setText(chooser.getSelectedFile().getAbsolutePath());
				
			} else if (srcComp == submit) {
				
				// Validation des chemins entrés dans les champs.
				
				// Vérification validité des entrées.
				if (controlFields()) try {
					
					// TODO Transfert pack de ressources -> pack de textures
					
				} catch (Throwable t) {
					
					MessageBox.showThrowableBox(t);
					
				}
				
			}
			
		}
		
		/**
		 * Méthode contrôlant si les chemins d'accès entrés dans les champs pointent sur
		 * des fichiers valides, et si les fichiers source etdestination ne sont pas identiques.
		 * Sinon, affichage de messages d'erreur en conséquence.
		 *  
		 * @return <code>true</code> si les chemins d'accès entrés dans les champs pointent sur
		 * des fichiers valides, et si les fichiers source etdestination ne sont pas identiques.
		 */
		public boolean controlFields() {
			
			// Texte champ source.
			String srcText = sourcePathField.getText();
			
			// Les fichiers source et destination sont ils identiques ?
			if (destPathField.getText().equals(srcText)) {
				
				MessageBox.showErrorBox("Les fichiers source et destination sont identiques.");
				
				return false;
				
			}
			
			// Le fichier source existe-t-il ?
			boolean srcExists = Files.isRegularFile(Paths.get(sourcePathField.getText()));
			
			
			if (!srcExists) {
				
				MessageBox.showErrorBox("Les fichiers source et destination n'existent pas.");
				
				return false;
				
			}
			
			return true;
		}
		
	}

}
